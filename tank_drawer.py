import generic_drawer


class TankDrawer(generic_drawer.GenericDrawer):
    def __init__(self, spriteGroup, tank, tankSprite, tankTurretSprite):
        super().__init__(spriteGroup, tankSprite, tankTurretSprite)
        self.tank = tank
        self.tankSprite = tankSprite
        self.tankTurretSprite = tankTurretSprite

    def draw(self):
        tankPos = self.tank.get_pos()
        self.tankSprite.moveToPoint(*tankPos)
        turretPos = tankPos + self.tank.getTurretDir()*8
        self.tankTurretSprite.moveToPoint(*turretPos)
        self.tankSprite.rotate(self.tank.get_tank_rotation())
        self.tankTurretSprite.rotate(self.tank.getTurretkRotation())






