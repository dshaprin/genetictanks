

class TankController:
    def __init__(self, tank, field):
        self.tank = tank
        self.field = field

    def rotate_turet_at_point(self, x, y):
        self.tank.rotate_turret_at_point(x, y)

    def moveForward(self):
        self.tank.moveForward();

    def moveBackward(self):
        self.tank.moveBackward();

    def rotateLeft(self):
        self.tank.rotateLeft()

    def rotateRight(self):
        self.tank.rotateRight()

    def shoot(self):
        self.tank.shoot()
