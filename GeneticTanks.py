import pygame
import random
import pickle
from field import Field
from tank import Tank
from tank_drawer import TankDrawer
from tank_controller import TankController
from sprite import Sprite
from bullet_drawer import BulletDrawer
from neural_tank_controller import NeuralTankController
from copy import deepcopy
import os

try:
    pygame.init()
    size = (800,600)
    screen = pygame.display.set_mode(size)
    pygame.display.set_caption("Genetic tanks")
    myfont = pygame.font.SysFont("monospace", 100)

    done = False
    gameColor = (20, 130,   50)
    clock = pygame.time.Clock()
    field = Field(*size)
    tanks_count = 8

    player = Tank(field, random.randint(50, size[0] - 50), random.randint(50, size[1]- 50))
    field.player = player

    for _ in range(0,tanks_count):
        field.tanks.append(Tank(field, random.randint(0, size[0]), random.randint(0, size[1])))
    all_sprites = pygame.sprite.Group()
    tankImage = pygame.transform.smoothscale(pygame.image.load('resources/sprites/Tank/Tank_r.png').convert_alpha(), (32,32))
    turretImage =  pygame.transform.smoothscale(pygame.image.load('resources/sprites/Tank/GunTurret_r.png').convert_alpha(), (16, 32))
    playerTankImage = pygame.transform.smoothscale(pygame.image.load('resources/sprites/Tank/Tank.png').convert_alpha(),                                         (32, 32))
    playerTurretImage = pygame.transform.smoothscale(pygame.image.load('resources/sprites/Tank/GunTurret.png').convert_alpha(), (16, 32))
    bulletImage =  pygame.transform.smoothscale(pygame.image.load('resources/sprites/Tank/Bullet.png').convert_alpha(), (8,8))

    tank_drawers = { t : TankDrawer(all_sprites, t, tankSprite = Sprite(tankImage), tankTurretSprite=Sprite(turretImage)) for t in field.tanks}
    player_drawer = TankDrawer(all_sprites, player, tankSprite = Sprite(playerTankImage), tankTurretSprite=Sprite(playerTurretImage))
    tank_drawers[player] = player_drawer
    bullet_drawers = {}

    tankController = TankController(player, field)

    enemy_controllers = [NeuralTankController(enemy, field) for enemy in field.tanks]

    moveUp = moveDown = rotateLeft = rotateRight = False

    round_time = 900


    def add_bullet(bullet):
        bullet_drawers[bullet] = BulletDrawer(all_sprites, bullet, Sprite(bulletImage))


    def remove_bullet(bullet):
        if bullet in bullet_drawers:
            bullet_drawers[bullet].dispose()
            del bullet_drawers[bullet]


    def remove_tank(tank):
        global frames_from_last_death
        frames_from_last_death = 0
        sorted_tank_controllers = sorted(enemy_controllers, key=lambda enemy: enemy.tank.get_points(), reverse=True)
        selected_tanks_controllers = select_tanks(sorted_tank_controllers, 2)

        tank_drawers[tank].dispose()
        del tank_drawers[tank]

        dead_tank_controller = next(dead_enemy for dead_enemy in enemy_controllers if dead_enemy.tank == tank)
        enemy_controllers.remove(dead_tank_controller)

        new_tank = Tank(field, random.randint(0, size[0]), random.randint(0, size[1]))
        field.tanks.append(new_tank)
        new_controller = deepcopy(selected_tanks_controllers[0])
        new_controller.tank = new_tank
        new_controller.combine_with(selected_tanks_controllers[1])

        enemy_controllers.append(new_controller)

        tank_drawer = TankDrawer(all_sprites, new_tank, tankSprite=Sprite(tankImage), tankTurretSprite=Sprite(turretImage))
        tank_drawers[new_tank] = tank_drawer


    def select_tanks(sorted_tank_controllers, count):
        selected_tanks_controllers = []
        while len(selected_tanks_controllers) != count:
            score_threshold = random.randint(0, 2 ** tanks_count - 1)
            accumulated_score = 0
            for i, t in enumerate(sorted_tank_controllers):
                power = 2 ** (tanks_count - i - 1)
                if accumulated_score + power >= score_threshold:
                    selected_tanks_controllers.append(t)
                    sorted_tank_controllers.remove(t)
                    break;
                else:
                    accumulated_score += power
        return selected_tanks_controllers


    field.addBulletEvent = add_bullet
    field.removeBulletEvent = remove_bullet
    field.removeTankEvent = remove_tank
    frames = 0
    frames_from_last_death = 0
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_w:
                    moveUp = True
                elif event.key == pygame.K_s:
                    moveDown = True
                elif event.key == pygame.K_a:
                    rotateLeft = True
                elif event.key == pygame.K_d:
                    rotateRight = True
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_w:
                    moveUp = False
                elif event.key == pygame.K_s:
                    moveDown = False
                elif event.key == pygame.K_a:
                    rotateLeft = False
                elif event.key == pygame.K_d:
                    rotateRight = False
                elif event.key == pygame.K_p:
                    for c in enemy_controllers:
                        c.print_weights()
                    here = os.path.dirname(os.path.abspath(__file__))

                    with open(os.path.join(here, "neural_data.dat"), 'wb') as output:
                        pickle.dump(list(map(lambda x: (x.wih, x.who), enemy_controllers)), output)
                elif event.key == pygame.K_r:
                    for t in field.tanks:
                        print(t.get_points())
                elif event.key == pygame.K_MINUS:
                    round_time /= 2
                    print(round_time)
                elif event.key == pygame.K_PLUS:
                    round_time *= 2
                    print(round_time)

            elif event.type == pygame.MOUSEBUTTONDOWN:
                tankController.shoot()

        if moveUp:
            tankController.moveForward()
        if moveDown:
            tankController.moveBackward()
        if rotateLeft:
            tankController.rotateLeft()
        if rotateRight:
            tankController.rotateRight()

        mouse_pos = pygame.mouse.get_pos()
        tankController.rotate_turet_at_point(*mouse_pos)

        player_drawer.draw()

        for _,drawer in tank_drawers.items():
            drawer.draw()

        bullets_to_remove = []

        for bullet, drawer in bullet_drawers.items():
            drawer.draw()
            blocks_hit_list = pygame.sprite.spritecollide(drawer.get_sprite(), [drawer.get_sprite() for _,drawer in tank_drawers.items() ], False)
            for hit in blocks_hit_list:
                hit_tank = filter(lambda drawer: drawer.get_sprite() == hit, tank_drawers.values())
                is_bullet_for_remove = False
                for h in hit_tank:
                    if h.tank != bullet.tank:
                        h.tank.get_hit(bullet)
                        is_bullet_for_remove = True
                if is_bullet_for_remove:
                    bullets_to_remove.append(bullet)

        for bullet_to_remove in bullets_to_remove:
            if bullet_to_remove in field.bullets:
                field.bullets.remove(bullet_to_remove)
            remove_bullet(bullet_to_remove)

        screen.fill(gameColor)
        label = myfont.render("HEALTH: " + str(player.get_health()), 1, (150, 150, 255))
        screen.blit(label, (100, 200))

        label = myfont.render("POINTS: " + str(player.get_points()), 1, (150, 150, 255))
        screen.blit(label, (100, 320))

        all_sprites.draw(screen)

        pygame.display.flip()
        for enemyController in enemy_controllers:
            enemyController.update()
        field.update()

        clock.tick(60)
        frames += 1
        """
        frames_from_last_death +=1
        if frames_from_last_death % round_time == 0:
            for tank in field.tanks:
                tank.set_points(0)
            sorted_tank_controllers = sorted(enemy_controllers, key=lambda enemy: enemy.tank.get_points())
            tank_controllers = select_tanks(sorted_tank_controllers, 8)
            for tank_controller in tank_controllers:
                field.remove_tank(tank_controller.tank)
        elif frames_from_last_death % 60 == 0:
            print(frames_from_last_death / 60)
        """

except:
    with open('neural_data.dat', 'wb') as output:
        here = os.path.dirname(os.path.abspath(__file__))

        with open(os.path.join(here, "neural_data.dat"), 'wb') as output:
            pickle.dump(list(map(lambda x: (x.wih, x.who), enemy_controllers)), output)
        raise


