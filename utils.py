import numpy as np
import math
import pygame
def get_rotation_matrix(angle):
    return np.asmatrix([[math.cos(angle), -math.sin(angle)],[math.sin(angle), math.cos(angle)]])


def get_pos_normalized( pos, size):
    pos_normalized = pos - size/ 2
    pos_normalized[0] /= size[0] / 2
    pos_normalized[1] /= size[1] / 2
    return pos_normalized
