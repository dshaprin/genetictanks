import pygame
import math
class Sprite(pygame.sprite.Sprite):
    def __init__(self, surface):
        super().__init__()
        self.image = surface.copy()
        self.original_image = surface.copy()
        self.rect = self.image.get_rect()

    def moveToPoint(self, x, y):
        width = self.rect.width
        height = self.rect.height
        self.rect.x = x - width / 2
        self.rect.y = y - height / 2

    def rotate(self, angle):
        oldCenter = self.rect.center
        new_image = pygame.transform.rotate(self.original_image, angle* 180.0 / math.pi )
        self.image = new_image
        self.rect = self.image.get_rect()
        self.rect.center = oldCenter


        