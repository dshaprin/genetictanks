import numpy as np
import random as rand

class Field(object):

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.size = np.asarray([width, height])
        self.tanks = []
        self.bullets = []
        self.addBulletEvent = lambda bullet: None
        self.removeBulletEvent = lambda bullet: None
        self.removeTankEvent = lambda tank : None
        self.mutation_rate = 7
        self.player = None

    def update(self):
        if self.player:
            self.player.update()
            if self.player.get_health() <= 0:
                self.player.set_health(100)
                self.player.set_pos([rand.randint(100, self.size[0]), rand.randint(100, self.size[1])])

        for tank in self.tanks:
            tank.update()
            if tank.get_health() <=0 :
                self.remove_tank(tank)

        for b in self.bullets:
            b.move()
            b.update()
            if not (0 < b.get_pos()[0] < self.width and 0 < b.get_pos()[1] < self.height):
                self.bullets.remove(b)
                self.removeBulletEvent(b)

    def add_bullet(self, bullet):
        self.bullets.append(bullet)
        self.addBulletEvent(bullet)

    def get_closest_bullets(self, pos, count = 8):
        sorted_bullets = sorted(self.bullets, key=lambda b: np.linalg.norm(b.get_pos() - pos ))
        return sorted_bullets[:count]

    def remove_tank(self, tank):
        self.tanks.remove(tank)
        self.removeTankEvent(tank)








