import numpy as np
import copy
import utils

class Bullet:
    class BulletState:
        def __init__(self, pos, direction):
            self.pos = pos
            self.dir = direction

    def __init__(self, pos, dir, tank ):
        self.current_state = Bullet.BulletState(pos, dir)
        self.prev_state = copy.deepcopy(self.current_state)
        self.tank = tank
        self.speed = 10

    def move(self):
        self.current_state.pos += self.speed * self.current_state.dir

    def update(self):
        self.prev_state = copy.deepcopy(self.current_state)

    def get_pos(self):
        return self.current_state.pos

    def get_dir(self):
        return self.current_state.dir

    def set_pos(self, pos):
        self.current_state.pos = pos

    def set_dir(self, dir):
        self.current_state.dir = dir

    def get_angle(self):
        return  np.arctan2(-1, 0) - np.arctan2(self.current_state.dir[1], self.current_state.dir[0]);

    def get_damage(self):
        return 40;

    def get_pos_normalized(self):
        return utils.get_pos_normalized(self.get_pos(), self.tank.field.size)
