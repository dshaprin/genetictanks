import pygame
import abc

class GenericDrawer(object):
    __metaclass__ = abc.ABCMeta
    def __init__(self, spriteGroup, *sprites):
        spriteGroup.add(sprites)
        self.sprites = sprites
        self.spriteGroup = spriteGroup

    @abc.abstractmethod
    def draw(self):
        return

    @abc.abstractmethod
    def get_sprite(self):
        return self.sprites[0]

    @abc.abstractmethod
    def dispose(self):
        for sprite in self.sprites:
            self.spriteGroup.remove(sprite)