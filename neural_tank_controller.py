import random
import numpy as np
import scipy
import scipy.special
import scipy.misc
import utils

class NeuralTankController:
    def __init__(self, tank, field):
        self.tank = tank
        self.field = field
        #15 tank 12 enemy 4 bullet
        self.inodes = len(self.field.tanks) * 6 - 3 + 8*3 + 18
        #if self.field.player:
        #    self.inodes += 6
        self.hnodes = 30
        self.onodes = 4
        self.activation_function = lambda x: scipy.special.expit(x)

        self.wih = np.random.normal(0.0, self.hnodes ** -0.5, (self.hnodes, self.inodes))
        self.who = np.random.normal(0.0, self.onodes ** -0.5, (self.onodes, self.hnodes))

    def update(self):
        enemies = [t for t in self.field.tanks if t != self.tank]
        if self.field.player:
            enemies.append(self.field.player)

        array = [self.tank.get_current_pos_normalized(),[self.tank.get_tank_rotation()/np.pi]]
        enemies_array = np.concatenate([[utils.get_pos_normalized(e.get_pos() - self.tank.get_pos(), self.tank.field.size),
                                         utils.get_pos_normalized(e.get_prev_pos() - self.tank.get_prev_pos(), self.tank.field.size),
                                         [e.get_tank_rotation()/np.pi, e.get_prev_tank_rotation()/np.pi]] for e in enemies if e != self.tank ])
        array.extend(enemies_array)

        if self.field.bullets:
            bullets_array = np.concatenate([[utils.get_pos_normalized(b.get_pos_normalized() - self.tank.get_pos(), self.tank.field.size),
                                             [b.get_angle()/np.pi]] for b in self.field.get_closest_bullets(self.tank.get_pos(), 8)])
            array.extend(bullets_array)

        input_vector = np.concatenate(array)
        input_vector = np.pad(input_vector, (0, self.inodes - len(input_vector)), 'constant', constant_values=0)

        hidden_inputs = np.dot(self.wih, input_vector)
        hidden_outputs = self.activation_function(hidden_inputs)

        final_inputs = np.dot(self.who, hidden_outputs)
        final_outputs = self.activation_function(final_inputs[:-1])
        final_outputs = np.append(final_outputs , final_inputs[-1])

        if final_outputs[0] > 0.8:
            self.tank.moveForward()
        if final_outputs[0] < -0.8:
            self.tank.moveBackward()
        if final_outputs[1] > 0.8:
            self.tank.rotateLeft()
        if final_outputs[1] < -0.8:
            self.tank.rotateRight()

        if final_outputs[2] > 0.8:
            self.tank.shoot()

        self.tank.set_turret_rotation(final_outputs[3])

    def combine_with(self, controller):
        for x in range(0, self.wih.shape[0]):
            for y in range(0, self.wih.shape[0]):
                if random.randint(0,100) < self.field.mutation_rate:
                    self.wih[x][y] = random.gauss(mu=0,sigma=-0.5)
                elif random.randint(0,1) == 1:
                    self.wih[x][y] = controller.wih[x][y]

        for x in range(0, self.who.shape[0]):
            for y in range(0, self.who.shape[0]):
                if random.randint(0,100) < self.field.mutation_rate:
                    self.who[x][y] = random.gauss(mu=0,sigma=-0.5)
                elif random.randint(0,1) == 1:
                    self.who[x][y] = controller.who[x][y]

    def print_weights(self):
        print(repr(self.wih))
        print(repr(self.who))
        print("------------------------------------------------------------------------")




"""ints = [ random.randint(1,100) for _ in range(0,10)]
        if ints[0] < 70:
            self.tank.moveForward()
        if ints [1] < 30:
            self.tank.moveBackward()
        if ints [3] < 30:
            self.tank.rotateLeft()
        if ints [4] < 20:
            self.tank.rotateRight()

        if ints [5] < 80:
            self.tank.rotate_turret_at_point(random.randint(0, self.field.width), random.randint(0, self.field.height))

        if ints[6] < 40:
            self.tank.shoot()
            """
