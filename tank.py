import copy
import numpy as np
import utils
import math
import random
from bullet import Bullet


class Tank(object):

    class TankState:
        def __init__(self, pos, turretDir, tankDir):
            self.pos = pos
            self.turretDir = turretDir
            self.tankDir = tankDir
            self.health = 100
            self.points = 0
            self.cooldown = 0

    def __init__(self,field, x, y):
        self.field = field
        self.tankState = Tank.TankState( np.asarray([x,y],dtype=np.float), np.asarray([0., -1.],dtype=np.float),  np.asarray([0.,-1.],dtype=np.float))
        self.prevTankState = copy.deepcopy(self.tankState)

    def rotate_turret_at_point(self, x, y):
        point = np.asarray([x,y],dtype=np.float)
        self.tankState.turretDir = np.asarray(point - self.tankState.pos,dtype=np.float)
        self.tankState.turretDir /= np.linalg.norm(self.tankState.turretDir)

    def get_turret_rotation_delta(self):
        return np.arctan2(self.tankState.turretDir[1], self.tankState.turretDir[0]) - np.arctan2(self.prevTankState.turretDir[1], self.prevTankState.turretDir[0])

    def get_tank_rotation_delta(self):
        return np.arctan2(self.tankState.tankDir[1], self.tankState.tankDir[0]) - np.arctan2(self.prevTankState.tankDir[1], self.prevTankState.tankDir[0])

    def get_tank_rotation(self):
        res = np.arctan2(-1, 0) - np.arctan2(self.tankState.tankDir[1], self.tankState.tankDir[0]);
        return res;

    def get_prev_tank_rotation(self):
        res = np.arctan2(-1, 0) - np.arctan2(self.prevTankState.tankDir[1], self.prevTankState.tankDir[0]);
        return res;

    def getTurretkRotation(self):
        res = np.arctan2(-1, 0) - np.arctan2(self.tankState.turretDir[1], self.tankState.turretDir[0]);
        return res;

    def set_turret_rotation(self, angle):
        self.tankState.turretDir[0] = math.sin(angle)
        self.tankState.turretDir[1] = -math.cos(angle)

    def getTankDir(self): return self.tankState.tankDir
    def getTurretDir(self): return self.tankState.turretDir
    def get_pos(self): return self.tankState.pos
    def get_prev_pos(self): return self.prevTankState.pos

    def setTankDir(self, dir):  self.tankState.tankDir = np.asarray(dir, dtype=np.float)
    def setTurretDir(self, dir):  self.tankState.turretDir = np.asarray(dir, dtype=np.float)
    def set_pos(self, pos):
        pos[0] = max(min(self.field.width - 16, pos[0] ), 16)
        pos[1] = max(min(self.field.height - 16, pos[1]), 16)
        self.tankState.pos = np.asarray(pos, dtype=np.float)

    def moveForward(self, amount = 4.0):
        self.set_pos(self.get_pos() + amount * self.getTankDir());

    def moveBackward(self, amount = 4.0):
        self.moveForward( amount = -amount)

    def rotateLeft(self, angle = 0.2):
        self.setTankDir(np.array(self.getTankDir() * utils.get_rotation_matrix(angle))[0])

    def rotateRight(self, angle = 0.2):
        self.rotateLeft(-angle)

    def shoot(self):
        if self.tankState.cooldown <=0:
            self.field.add_bullet(Bullet(self.get_pos() + self.getTurretDir() * 34, self.getTurretDir() * 1, self))
            self.tankState.cooldown = random.randint(15,20)

    def update(self):
        self.prevTankState = copy.deepcopy(self.tankState)
        self.tankState.cooldown -= 1

    def get_hit(self, bullet : Bullet):
        self.tankState.health -= bullet.get_damage()
        bullet.tank.add_points(bullet.get_damage())

    def add_points(self, points):
        self.tankState.points += points

    def set_points(self, points):
        self.tankState.points = 0

    def reduce_points(self, points):
        if self.tankState.points > 10:
            self.tankState.points -= points

    def get_points(self):
        return self.tankState.points

    def get_health(self):
        return self.tankState.health

    def set_health(self, health):
        self.tankState.health = health

    def get_current_pos_normalized(self):
        return utils.get_pos_normalized(self.get_pos(), self.field.size)

    def get_prev_pos_normalized(self):
        return utils.get_pos_normalized(self.get_prev_pos(), self.field.size)

