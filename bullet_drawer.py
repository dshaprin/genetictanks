import generic_drawer


class BulletDrawer(generic_drawer. GenericDrawer):
    def __init__(self, spriteGroup, bullet, bulletSprite):
        super().__init__(spriteGroup, bulletSprite)
        self.bullet = bullet
        self.bulletSprite = bulletSprite
        self.bulletSprite.rotate(bullet.get_angle())
        self.spriteGroup = spriteGroup

    def draw(self):
        bulletPos = self.bullet.get_pos()
        self.bulletSprite.moveToPoint(*bulletPos)
